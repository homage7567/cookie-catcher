const path = require('path');

/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  swcMinify: true,
  compress: true,
  optimizeFonts: true,
  images: {
    unoptimized: true
  },
  compiler: {
    removeConsole: true
  },
  experimental: {
    optimisticClientCache: true,
    swcMinify: true
  },
  sassOptions: {
    includePaths: [path.join(__dirname, 'styles')]
  }
};

module.exports = nextConfig;
